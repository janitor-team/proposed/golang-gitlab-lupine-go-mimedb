package mimedb

import (
	"mime"
	"testing"
)

func TestLoadTypes(t *testing.T) {
	if mime.TypeByExtension(".webmanifest") != "" {
		t.Log("We have .webmanifest BEFORE loading the DB. Passing would be false negative!")
		t.Fail()
	}

	if err := LoadTypes(); err != nil {
		t.Logf("Unexpected error loading types: %s", err)
		t.Fail()
	}

	if mime.TypeByExtension(".webmanifest") != "application/manifest+json" {
		t.Logf("A MIME type for .webmanifest was not found")
		t.Fail()
	}
}
